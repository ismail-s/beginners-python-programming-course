---
author: Ismail S
title: Beginners Programming Course
subtitle: Lesson one
date: November 2018
---

# Who am I?

- Studied maths & physics at Leeds uni
- Learnt to program in my spare time, initially via Youtube
- Got a job at a software consultancy
- Gets paid money to program

# Who are you?

Introductions

# Comms

Email, WhatsApp?

# Why am I doing this?

- I want to see if I can teach people to program
- To introduce you guys to a new skill (maybe a new career)

# What we will cover

- Basics of programming in Python eg:
    - Numbers, strings etc
    - Variables
    - Functions
    - Lists, dictionaries
- Making basic programs

# What we won't cover

- Classes (unless we do well with everything else)
- Making websites
- Making apps
- Making anything with a graphical interface
- Loads of other stuff
- But you'll take the first steps to being able to do these things

# How I'll teach

- We'll work through [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/) (with a few tweaks/additions)
- Work at your pace, but ideally everyone stays roughly together
- At some points, I'll present some stuff lecture-style
- I go around and help people

# What I expect of you

- Be respectful (of everyone).
    - This includes being on time (let me know in advance if you'll be late).
- Ask questions, no matter how basic they may seem (but also try and figure things out yourself)
- I may ask you to look things up yourselves - this is intentional, I'm not being lazy

# Homework?

- This isn't school
- But doing stuff outside of the lessons _will_ help. A lot.
- You can only help yourself.

# Exam

- There isn't one
- But depending on how things go, I may set a final (challenging-ish) exercise

# Warning

- Programming is **hard** (it requires you to think)
- But, if done right, it can be enjoyable/addictive
- Programming is not for everyone (but give it a decent shot before giving up)
- However, _software is eating the world_

# A little bit of background

# What are computers?

- Machines that follow instructions
- Example instructions:
    - Add these 2 numbers together
    - Check if this number is equal to 0
    - Go (jump) to another instruction
    - Store this number in this place in memory

---

- The instructions computers follow aren't too fancy or complicated
- But computers can perform these instructions very fast
    - By fast, we mean [billions of instructions a second](https://en.wikipedia.org/wiki/Instructions_per_second#Timeline_of_instructions_per_second) (1 billion = 1 000 000 000)
- And computers don't get tired or complain. Ever.

# What is programming?

- Writing the instructions that computers can then follow (execute)
- The language that computers understand is very boring and tedious to write instructions in
- So we use easier languages to write the instructions in (like Python)

---

- Then we either:
  - Run this through another program to turn it into the instructions a computer understands. Like translating from eg French to English. Except we get a program to do it for us.
  - Run a virtual computer on top of the actual computer. The virtual computer understands our instructions.
    - I know, it sounds crazy, but it does work
    - This is how we'll run our Python code
- A program is just a bunch of instructions for a computer to follow

# What is Python?

- A good programming language for beginners
- One of the most popular programming languages in industry
- Named after Monty Python

# Self-directed part

# Now let's go over what you learned

# Numbers

```python
>>> 1 + 1
2
>>> 1.1 + 2
3.1
>>> 3 - 2
1
```

---

```python
>>> 5 * 2
10
>>> 5.5 * 2
11.0
```

---

```python
>>> 10 / 2
5
>>> 7 / 2
3.5
>>> 7 // 2
3
```

# Strings

```python
>>> 'this is a string'
'this is a string'
>>> "this is a string"
'this is a string'
```

---

```python
>>> 'hello' * 2
'hellohello'
>>> 'hello' + ' ' + 'world'
'hello world'
```

# Variables

```python
>>> blah = 50
>>> blah
50
>>> blah = 'something else'
>>> blah
'something else'
```

---

```python
>>> first  = 'something'
>>> second = 'else'
>>> first + " " + second
'something else'
```

# Print function

- The print function takes something and prints it to the screen
- `print('like this')`
- `print(50)`
- `print('or even ' + 'like this')`

# Other functions

- `len` takes a string and returns the length
- `input` asks the user for input and returns it
- `int` turns things into an integer
- `str` turns things into a string

# Example code

```python
x = "This is some Python code"
print(x)

print("And this is some more Python code")
some_input = input()
print("You just typed: " + some_input)
```
# Lesson 2 - Flow Control

# Booleans

```python
>>> True
True
>>> False
False
>>> x = True
>>> x
True
```

---

```python
>>> True == True
True
>>> False == True
False
>>> True != False
True
```

# Comparing numbers and strings

---

```python
>>> 23 == 'a string'
False
>>> 23 == '23'
False
>>> 23 == int('23')
True
```

# Comparing numbers

---

```python
>>> 2 < 4
True
>>> 4 <= 4
True
>>> 4 < 4
False
>>> 3.2 > 3.123
True
```

# `and`, `or` and `not`

# `and`

```python
>>> True and True
True
>>> True and False
False
>>> False and False
False
```

---

# Truth table

|First|Second|Result|
|-----|------|------|
|`True`|`True`|`True`|
|`True`|`False`|`False`|
|`False`|`True`|`False`|
|`False`|`False`|`False`|

# `or`

```python
>>> True or True
True
>>> True or False
True
>>> False or False
False
```

---

# Truth table

|First|Second|Result|
|-----|------|------|
|`True`|`True`|`True`|
|`True`|`False`|`True`|
|`False`|`True`|`True`|
|`False`|`False`|`False`|

# `not`

```python
>>> not True
False
>>> not False
True
```

---

# Truth table

|First|Result|
|-----|------|
|`True`|`False`|
|`False`|`True`|

# Examples

```python
>>> x = 5
>>> 1 < x and x < 7
True
>>> not (1 < x)
False
>>> (x == 4) or (not (x >= 3))
False
```

# Control Flow

# `if` statements

```python
>>> name = 'Ismail'
>>> if 'Ismail' == name:
	print('My name is Ismail!')


My name is Ismail!
```

---

```python
>>> name = 'Adam'
>>> if 'Ismail' == name:
	print('My name is Ismail!')
else:
	print('My name is not Ismail')


My name is not Ismail
```

---

```python
>>> name = 'Mohammed'
>>> if 'Ismail' == name:
	print('My name is Ismail!')
elif len(name) == 8:
	print('My name is 8 letters long')
else:
	print('My name is not Ismail and doesn\'t have 8 letters')


My name is 8 letters long
```

# `while` loops

```python
>>> x = 0
>>> while x < 4:
	print('x is ' + str(x))
	x = x + 1


x is 0
x is 1
x is 2
x is 3
```

---

```python
>>> while True:
	print('Hello!')


Hello!
Hello!
Hello!
Hello!
Hello!
...
```

---

```python
>>> while True:
	number = input('Please enter a number: ')
	if int(number) == 12:
		print('You guessed the number!')
		break
	print('You guessed incorrectly. Try again.')


Please enter a number: 1
You guessed incorrectly. Try again.
Please enter a number: 3
You guessed incorrectly. Try again.
Please enter a number: 12
You guessed the number!
```

---

```python
>>> x = 0
>>> while x < 4:
  print('x is ' + str(x))
  print('x is less than 4')
  x = x + 1
  if x > 2:
    continue
  print('x is also less than 2')


x is 0
x is less than 4
x is also less than 2
x is 1
x is less than 4
x is also less than 2
x is 2
x is less than 4
x is 3
x is less than 4
```

---

# `for` loops

```python
>>> for i in range(5):
	print(i)


0
1
2
3
4
```

---

This is equivalent to:

```python
>>> i = 0
>>> while i < 5:
	print(i)
	i = i + 1


0
1
2
3
4
```

# Modules

- Modules are collections of functions and other stuff
- You have to import a module, then you can use the things that are in it


# Importing modules

```python
>>> import sys
>>> sys.exit()
```

---

```python
>>> from sys import exit
>>> exit()
```

---

```python
>>> from sys import *
>>> exit()
```

Importing in this way can make code confusing

# Lesson 3 - Functions

# Example function

```python
def hello():
  print("We're in a function")
  print("And we're still in it")
  print('And this is the last line')
```

# What can functions do?

---

Functions can:

- take 1 or more inputs
- return 1 or more outputs (more than 1 output will be covered later)
- do anything that can be done outside functions
    - eg print to screen, get input from the user, add numbers etc

---

```python
def anotherFunc(x, y):
  print('We're inside this function')
  return x + (y * y)
```

# `None`

- `None` is the only value of type `NoneType`
- If a function doesn't execute a `return` statement, then `None` is implicitly returned

---

```python
>>> def nn():
	print('test')


>>> x = nn()
test
>>> print(x)
None
```

# Keyword arguments

```python
>>> print('this', 'gets', 'spaced', 'out', sep = '   ')
this   gets   spaced   out
```

# Local & global scope

- Variables assigned inside a function exist in the _local scope_ of that function
- Variables assigned outside all functions exist in the _global scope_

---

- Local scopes are created every time a function is _called_
- Scopes sound confusing at first, but with practice, they become intuitive

---

```python
x = 5

def some_func():
  print(x) # Will print 5
```

---

```python
x = 5

def some_func():
  x = 7

print(x) # Will print 5
```

Try not to do this to avoid confusion

---

```python
x = 5

def some_func():
  global x
  x = 7

print(x) # Will print 7
```

In practice, we rarely use `global` as it makes code confusing

---

```python
def some_func():
  x = 7
  some_other_func()

def some_other_func():
  print(x)

some_func() # Get an exception
# as some_other_func doesn't
# have x in it's local scope
```

# Exception handling

---

```python
def foo(x, y):
  try:
    return x / y
  except ZeroDivisionError:
    print('Tried to divide by zero')
    print('Returning 0')
    return 0
```

---

```python
def bar(x, y, operation):
  try:
    return operation(operation(x, y), y)
  except Exception:
    print('Got an exception whilst trying to run operation')
```
